var q_array = [

[["Hvilken berømt romersk struktur tiltrækker turister og lokale, og hver april dekoreres med pink azalea?"], [true], ["Pantheon", "Den Spanske Trappe", "Notre Dame", "Trevi-fontænen"]], //2
[["Hvilket dyr er beskyttet af loven i Rom?"], [true], ["Hunde", "Duer", "Katte", "Køer"]], //3
[["Hvornår blev Rom hovedstaden af Italien?"], [true], ["1871", "1781", "1964", "1034"]], //1
[["Hvor mange indbyggere har Rom?"], [true], ["ca. 6 mio.", "ca. 4 mio", "ca. 3 mio.", "ca. 8 mio."]], //3
[["Hvor meget pasta spiser en italiener gennemsnitligt om året?"], [true], ["5kg", "11kg", "20kg", "50kg"]], //20kg
[["Hvorfor redte man seng i det gamle rom?"], [true], ["For at gøre sengen pæn", "For at aftrykket af ens krop ikke kunne bruges til sort magi", "Så kattene kunne ligge behageligt i løbet af dagen", "Så onde ånder ikke lægge sig i deres seng"]]

];

var i_array = [

"Rom er verdenskendt for sin velbevarede, gamle arkitektur. Blandt de mange værker står Colosseum, <a href=\"http://en.wikipedia.org/wiki/Pantheon,_Rome\">Pantheon</a> og Den Spanske Trappe som nogle af de mest kendte. De mange værker står stadig, bl.a. fordi de er konstrueret af materialer, som ikke er nemme at genbruge. Mange store bygningsværker bygget af sten, kunne rives ned igen for at bruge stenene til at bygge nye bygninger. <a href=\"http://en.wikipedia.org/wiki/Colosseum\">Colosseum</a> er den store arena, hvor indbyggere fra hele Rom ville samles for at nyde underholdning såsom gladiatorkampe og hestevæddeløb. Den store bygning er oval, med arena i midten og siddepladser langs indersiden, med plads til op mod 80.000 mennesker. <a href=\"http://en.wikipedia.org/wiki/Spanish_Steps\">Den Spanske Trappe</a> er et sæt trapper i Rom, som har taget navn efter den spanske ambassade, som trappen fører op til. Omkring juletid fremvises en krybbe fra 1900-tallet på trinene af Den Spanske Trappe, og om foråret vil den kunne ses dekoreret med blomster i krukker. I mere moderne tider fungerer trapperne som et lille blomstermarked, og er typisk fyldt med mennesker.",

"Der findes en uddød hunderace, kaldet <a href=\"http://en.wikipedia.org/wiki/Molossus_(dog)%20\">molossus</a>, som fandtes i Rom i et godt stykke tid. De var beundret af romerne og grækerne, for værende store og stærke. Denne hund er meget berømt i Italien. </br><a href=\"https://web.archive.org/web/20080501093143/http://www.hsus.org/pets/issues_affecting_our_pets/feral_cats/feral_cats_frequently_asked_questions.html\">Vilde katte</a> færdes rundt omkring i bl.a. storbysområder. Antallet af vilde katte kendes ikke præcist, men i Rom er der ca. 30 tusinde. </br>Nogle vilde katte lever alene, men de fleste lever i flokke med et afmærket territorium. Nogle særdeles kendte flokke af vilde katte er de, der bor i nærheden af Colosseum og Forum Romanum. </br>Der er ikke en hel del duer i Rom længere, og det er blevet ulovligt at fodre duerne på trods af at mange turister stadig gør det. </br>Du finder nok ikke mange køer i Rom, men der fandtes engang en ko, kaldet den <a href=\"http://en.wikipedia.org/wiki/Sacred_bull\">Hellige Tyr</a> som betød meget for romerne.",

"<a href=\"http://en.wikipedia.org/wiki/Italian_unification\">Den italienske forening</a> - på italiensk: Risorgimento - var den politiske og sociale bevægelse, der samlede de forskellige stater på den italienske halvø til én nation, Kongedømmet Italien. </br>Dette foregik i det 19. århundrede. </br>Der er ikke 100% enighed om de præcise datoer for starten og slutningen af denne periode, men de fleste forskere er enige om at foreningen af Italien begyndte i 1815 med <a href=\"http://da.wikipedia.org/wiki/Wienerkongressen\">Wienerkongressen</a> og slutningen af det Napoleonske styre og blev afsluttet i 1871, da Rom blev hovedstad i Kongedømmet Italien. </br>Dog blev ikke alle de omliggende områder inkluderet før Saint-Germain-traktaten blev underskrevet efter 1. verdenskrig.",

"De tidligste <a href=\"http://www.answers.com/Q/Who_were_the_first_civilized_inhabitants_of_Rome\">bosættere i Italien</a> ankom i forhistorisk tid. Disse grupper var <a href=\"http://www.denstoredanske.dk/Geografi_og_historie/Antikken/Romerriget/Romerriget_til_ca._27_f.Kr./latinere\">Latinere</a>, som bosatte sig på en af Roms syv bakker, hvilket gør dem til de første romerske bosættere. <br>Den anden gruppe var grækere som etablerede kolonier langs Syd-Sicilien og -Italien. <br>Den tredje og sidste gruppe var etruskerne som leve i Norditalien. De havde en stor indflydelse på udviklingen af den romerske civilisation. <br>Det var fra etruskerne, romerne lærte brugen af bl.a. alfabetet. <br>Efterhånden kom flere til og nu er der omtrent 3.8 millioner indbyggere i Rom.",

"Italienere elsker pasta. Det ved de fleste. Hver gang italienere immigrerede førhen, <a href=\"http://www.lifeinitaly.com/food/pasta-history.asp\">bragte de altid pasta med sig</a>. Pasta går hundrede, hvis ikke tusinder, af år tilbage. Mange børn får i skolen at vide, at Marco Polo bragte pasta hjem fra sine rejser til Kina med sig til Italien, men det har vist sig blot at være en skrøne. Nudler fandtes allerede i Italien i Marco Polos tid. <br><br>De er så glade for pasta i Italien at der endda findes et <a href=\"http://www.museodellapasta.it/changelang.php?lang=en%20\">pastamuseum</a> i Rom med 11 forskellige udstillingshaller, hvor besøgende kan gennemleve pastaens historie, ændringer i pastaens produktion, dens ernæringsmæssige betydning og endda hvordan pasta er blevet portrætteret i kunst gennem tiderne. Desuden spiser den gennemsnitlige italiener omkring <a href=\"http://www.funtrivia.com/en/geography/rome-17050.html\">tyve kilo</a> pasta om året.",

"Romerne fik størstedelen af deres <a href=\"http://www.roman-empire.net/religion/superstitions.html%20\">overtro</a> fra etruskerne. De kunne læse tegn sendt til dem fra guderne og gjorde det til en videnskab. De ville se i fremtiden ved at undersøge indvolde af ofrede dyr, hvor leveren spillede en stor rolle til dette specifikke formål. De ville observere lyn og fortolke deres mening. Generelt prøvede de at give ethvert usædvanligt fænomen en videnskabelig mening. <br><br>Romerne var overbevist om at alting - planter, vand, klipper, dyr og selv møbler - besad en ånd. Forskellige møbler kunne have forskellige ånder eller dæmoner i sig, bl.a. kunne aftrykket af ens krop på sengen bruges til sort magi. Denne overtro om ånder i objekter gjaldt dog mest for sten. Især hvis disse sten var lagt mellem to mænds ejendom, som en grænse."

];

var correct_answers = [
1,
2, 
0,
1, 	
2,
1

];
var e_text = [


];

var answer_array = [];


var margin_height = 100.8;
var focused = 0;

function get_title(id) {
	if(id < q_array.length)
		return q_array[id][0][0];
	else
		return "Request is out of bounds"+id;
}

function get_image(id) {
	if(id < q_array.length) {
		if(q_array[id][1][0] == true)
			return "img/"+id+".png";
		else
			return q_array[id][1][0];
	}
	else
		return "Request is out of bounds"+id;
}

function get_answer(id, ans_num) {
	if(id < q_array.length)
		return q_array[id][2][ans_num];
	else
		return "Request is out of bounds"+id+", "+ans_num;
}

function get_info(id) {
	if(id < i_array.length)
		return i_array[id];
	else
		return "Request is out of bounds"+id;
}

function select_answer(q, a)
{
	answer_array[q] = a;
	for(var asd = 0; asd < 4; asd++)
	{
		document.getElementsByClassName("answer_container")[q].children[asd].className = document.getElementsByClassName("answer_container")[q].children[asd].className.replace("selected", "");
	}
	document.getElementsByClassName("answer_container")[q].children[a].className += " selected";

	slide_to(focused+1);

}
function check_answers() {
	var cr = 0; // corret
	var wr = 0; // wrong
	for(var d = 0; d < correct_answers.length; d++)
	{
		
		if(answer_array[d] == correct_answers[d]) {
			document.getElementsByClassName("answer_container")[d].children[correct_answers[d]].style.background = "#67DB58";
			cr++;
		}
		else {
			if(answer_array[d] == -1)
				for(var e = 0; e < 4; e++)
					document.getElementsByClassName("answer_container")[d].children[e].style.background = "#bf4040";
			document.getElementsByClassName("answer_container")[d].children[correct_answers[d]].style.background = "#67DB58";
			if(answer_array[d] != -1)
				document.getElementsByClassName("answer_container")[d].children[answer_array[d]].style.background = "#bf4040";

			wr++;
		}
	}
	var len = document.getElementsByClassName("question").length;
	for (p = 0; p < len; p++) {
		document.getElementsByClassName("question")[p].setAttribute("onClick", "");
	}


	document.getElementById("results").innerHTML = cr+" korrekte svar. "+wr+" forkerte svar.";
	slide_to(0);
}
function build() {
	if(i_array.length != q_array.length || q_array.length != correct_answers.length )
		return "Build Error. Non-Matching build arrays.";
	
	for(var i = 0; i < q_array.length; i++) {
		
		answer_array[i] = -1;

		//insert image
		var image = document.createElement("img");
		image.src = get_image(i);
		image.className += " image_container";
		

		//insert the infotext

		var infotext = document.createElement("div");
		var paragraph = document.createElement("p");
		paragraph.innerHTML = get_info(i);
		infotext.appendChild(paragraph);
		infotext.className += " infotext_container";



		//insert the questions

		var container = document.createElement("div");
		container.className = "question_container";
		var headline = document.createElement("h3");
		headline.innerHTML = get_title(i);
		container.appendChild(headline);
		var q_container = document.createElement("div");
		q_container.className = "answer_container";

		for(k = 0; k < 4; k++)
		{
			var q = document.createElement('button');
			q.className = "question";
			q.innerHTML = get_answer(i, k);
			get_answer(i, k).length > 20 ? q.style.fontSize = "14px" : "";
			q.setAttribute("onClick", "select_answer("+i+","+k+");")
			q_container.appendChild(q);
		}
		var econtain = document.createElement("p");
		econtain.className = "extratext_container";
		container.appendChild(econtain);
		container.appendChild(q_container);





		//insert the buttons
		var button = document.createElement("input");
		button.className="button-circle";
		button.type = "button";
		button.setAttribute("onClick", "slide_to("+i+")");
		button.value = i+1;
		button.id = i+"button";
		//insert
		document.getElementsByClassName("left-buttons")[0].appendChild(button);
		document.getElementsByClassName("quest_anim")[0].appendChild(container);
		document.getElementsByClassName("img_anim")[0].appendChild(image);
		document.getElementsByClassName("text_anim")[0].appendChild(infotext);
	}
	return true;
}

function slide_next() {
	slide_to(focused+1)
}

function slide_prev() {
	slide_to(focused-1)
}

function slide_to(id) {
	if (id < q_array.length && id >= 0) {
		var anims = document.getElementsByClassName("anim_div");
		for(var x = 0; x < anims.length; x++) {
			anims[x].style.marginTop = margin_height*id*-1+"%";
		}
		var hlit = document.getElementById(focused+"button").className; 	
		hlit = hlit.replace("highlit", "");
		document.getElementById(focused+"button").className = hlit;
		document.getElementById(id+"button").className += " highlit";
		focused = id;
	}
	
}

function init() {
	var doc = build();
	document.getElementById(0+"button").className += " highlit";
	if(!doc)
		document.body.innerHTML=doc;
}

function eval_answers() {

}
